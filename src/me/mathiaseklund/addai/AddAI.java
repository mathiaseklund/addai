package me.mathiaseklund.addai;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;

public class AddAI {

	public static HashMap<String, Long> cooldown = new HashMap<String, Long>();

	/**
	 * Add AI to target entity
	 * 
	 * @param player
	 */
	public static void addAI(Player player) {
		Main main = Main.getMain();
		Entity e = getTargetEntity(player);
		if (e != null) {
			if (!hasCooldown(player.getUniqueId().toString())) {
				Location loc = e.getLocation();
				loc.setDirection(e.getLocation().getDirection());
				Vector vec = e.getVelocity();
				String cname = e.getCustomName();
				EntityDamageEvent lastDamageCause = e.getLastDamageCause();
				List<Entity> passenger = e.getPassengers();
				EntityType type = e.getType();
				int ticksLived = e.getTicksLived();
				int fireticks = e.getFireTicks();
				Entity vehicle = e.getVehicle();
				e.remove();

				LivingEntity ent = (LivingEntity) Bukkit.getWorld(loc.getWorld().getName()).spawnEntity(loc, type);
				int seconds = main.getConfig().getInt("cooldown");
				long now = Calendar.getInstance().getTimeInMillis();
				long cd = now;
				cd += (seconds * 1000);
				cooldown.put(player.getUniqueId().toString(), cd);
				ent.setVelocity(vec);

				Util.debug("entyaw: " + ent.getLocation().getYaw());
				if (cname != null) {
					ent.setCustomName(cname);
				}
				if (lastDamageCause != null) {
					ent.setLastDamageCause(lastDamageCause);
				}
				if (passenger != null) {
					for (Entity pas : passenger) {
						ent.addPassenger(pas);
					}
				}

				ent.setTicksLived(ticksLived);
				ent.setFireTicks(fireticks);

				if (vehicle != null) {
					vehicle.addPassenger(ent);
				}

				Util.message(player, main.getMessages().getString("addai.added").replaceAll("%type%", type.toString()));
			} else {
				Util.message(player, main.getMessages().getString("addai.cooldown"));
			}
		} else {
			Util.message(player, main.getMessages().getString("addai.noentity"));
		}
	}

	/**
	 * Remove AI from target entity
	 * 
	 * @param player
	 */
	public static void removeAI(Player player) {
		Main main = Main.getMain();
		Entity e = getTargetEntity(player);
		if (e != null) {
			if (!hasCooldown(player.getUniqueId().toString())) {
				Location loc = e.getLocation();
				loc.setDirection(e.getLocation().getDirection());
				Vector vec = e.getVelocity();
				String cname = e.getCustomName();
				EntityDamageEvent lastDamageCause = e.getLastDamageCause();
				List<Entity> passenger = e.getPassengers();
				EntityType type = e.getType();
				int ticksLived = e.getTicksLived();
				int fireticks = e.getFireTicks();
				Entity vehicle = e.getVehicle();
				e.remove();

				LivingEntity ent = (LivingEntity) Bukkit.getWorld(player.getWorld().getName()).spawnEntity(loc, type);
				ent.setAI(false);
				int seconds = main.getConfig().getInt("cooldown");
				long now = Calendar.getInstance().getTimeInMillis();
				long cd = now;
				cd += (seconds * 1000);
				cooldown.put(player.getUniqueId().toString(), cd);
				ent.setVelocity(vec);

				Util.debug("entyaw: " + ent.getLocation().getYaw());
				if (cname != null) {
					ent.setCustomName(cname);
				}
				if (lastDamageCause != null) {
					ent.setLastDamageCause(lastDamageCause);
				}
				if (passenger != null) {
					for (Entity pas : passenger) {
						ent.addPassenger(pas);
					}
				}

				ent.setTicksLived(ticksLived);
				ent.setFireTicks(fireticks);

				if (vehicle != null) {
					vehicle.addPassenger(ent);
				}
				Util.message(player,
						main.getMessages().getString("removeai.removed").replaceAll("%type%", type.toString()));
			} else {
				Util.message(player, main.getMessages().getString("removeai.cooldown"));
			}
		} else {
			Util.message(player, main.getMessages().getString("removeai.noentity"));
		}
	}

	/**
	 * Get target player
	 * 
	 * @param player
	 * @return
	 */
	public static Player getTargetPlayer(final Player player) {
		return getTarget(player, player.getWorld().getPlayers());
	}

	/**
	 * Get target entity
	 * 
	 * @param entity
	 * @return
	 */
	public static Entity getTargetEntity(final Entity entity) {
		return getTarget(entity, entity.getWorld().getEntities());
	}

	/**
	 * Get target entity
	 * 
	 * @param entity
	 * @param entities
	 * @return
	 */
	public static <T extends Entity> T getTarget(final Entity entity, final Iterable<T> entities) {
		if (entity == null)
			return null;
		T target = null;
		final double threshold = 1;
		for (final T other : entities) {
			final Vector n = other.getLocation().toVector().subtract(entity.getLocation().toVector());
			if (entity.getLocation().getDirection().normalize().crossProduct(n).lengthSquared() < threshold
					&& n.normalize().dot(entity.getLocation().getDirection().normalize()) >= 0) {
				if (target == null || target.getLocation().distanceSquared(entity.getLocation()) > other.getLocation()
						.distanceSquared(entity.getLocation()))
					target = other;
			}
		}
		return target;
	}

	/**
	 * Check if player has a cooldown
	 * 
	 * @param uuid
	 * @return
	 */
	public static boolean hasCooldown(String uuid) {
		if (cooldown.containsKey(uuid)) {
			long cd = cooldown.get(uuid);
			long now = Calendar.getInstance().getTimeInMillis();
			if (now >= cd) {
				cooldown.remove(uuid);
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
}
