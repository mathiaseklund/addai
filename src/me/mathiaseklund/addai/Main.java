package me.mathiaseklund.addai;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	static Main main;

	// Files
	File messagesFile;
	FileConfiguration messages;

	/**
	 * Get instance of Main class
	 * 
	 * @return Main class
	 */
	public static Main getMain() {
		return main;
	}

	/**
	 * Plugin is enabled
	 */
	public void onEnable() {
		main = this;

		LoadYMLFiles();
		RegisterListeners();
		RegisterCommands();
	}

	/**
	 * Loa default .yml files
	 */
	void LoadYMLFiles() {
		this.saveDefaultConfig();

		/**
		 * Messages file
		 */
		messagesFile = new File(getDataFolder(), "messages.yml");
		if (!messagesFile.exists()) {
			messagesFile.getParentFile().mkdirs();
			saveResource("messages.yml", false);
		}

		messages = new YamlConfiguration();
		try {
			messages.load(messagesFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save messages file
	 */
	public void saveMessages() {
		try {
			messages.save(messagesFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Get Messages config
	 * 
	 * @return
	 */
	public FileConfiguration getMessages() {
		return this.messages;
	}

	/**
	 * Register Event Listener classes
	 */
	void RegisterListeners() {
	}

	/**
	 * Register command executors
	 */
	void RegisterCommands() {
		getCommand("addai").setExecutor(new CommandAddAI());
		getCommand("removeai").setExecutor(new CommandRemoveAI());
	}
}
