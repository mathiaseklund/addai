package me.mathiaseklund.addai;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Util {

	/**
	 * Send a debug message
	 * 
	 * @param message
	 *            Message to be sent
	 */
	public static void debug(String message) {
		Main main = Main.getMain();
		if (main.getConfig().getBoolean("debug")) {
			System.out.println("[DEBUG PLUGIN] - " + message);
		}
	}

	/**
	 * capitalize the first letter of a word
	 * 
	 * @param original
	 * @return
	 */
	public static String capitalizeFirst(String original) {
		if (original == null || original.length() == 0) {
			return original;
		}
		return original.substring(0, 1).toUpperCase() + original.substring(1);
	}


	/**
	 * Format a money value with commas.
	 * 
	 * @param value
	 *            amount to format.
	 * @return formatted string
	 */
	public static String FormatMoney(double value) {
		String output = NumberFormat.getNumberInstance(Locale.US).format(value);
		return output;
	}

	/**
	 * Split seconds into Hours, minutes, seconds
	 * 
	 * @param biggy
	 * @return
	 */
	public static int[] splitSeconds(BigDecimal seconds) {
		long longVal = seconds.longValue();
		int hours = (int) longVal / 3600;
		int remainder = (int) longVal - hours * 3600;
		int mins = remainder / 60;
		remainder = remainder - mins * 60;
		int secs = remainder;

		int[] ints = { hours, mins, secs };
		return ints;
	}

	/**
	 * Check if string is an integer
	 * 
	 * @param str
	 * @return true/false
	 */
	public static boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		if (str.isEmpty()) {
			return false;
		}
		int i = 0;
		int length = str.length();
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c < '0' || c > '9') {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check if string is a Double
	 * 
	 * @param str
	 * @return true/false
	 */
	public static boolean isDouble(String str) {
		final Pattern DOUBLE_PATTERN = Pattern
				.compile("[\\x00-\\x20]*[+-]?(NaN|Infinity|((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)"
						+ "([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|"
						+ "(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))"
						+ "[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*");
		return DOUBLE_PATTERN.matcher(str).matches();
	}

	/**
	 * Send a message to a command sender
	 * 
	 * @param player
	 *            CommandSender that receives message
	 * @param message
	 *            message to be sent
	 */
	public static void message(CommandSender sender, String message) {
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));

	}

	/**
	 * Get vanilla name of itemstack
	 * 
	 * @param itemStack
	 *            itemstack to get name from
	 * @return name of itemstack
	 *//*
	public static String getName(ItemStack itemStack) {
		final String[] item = { itemStack.getType().name() };
		ReflectionUtil.newCall().getMethod(MinecraftReflectionProvider.CRAFT_ITEMSTACK, "asNMSCopy", ItemStack.class)
				.get().passIfValid(reflectionMethod -> {
					Object nmsItemStack = reflectionMethod.invokeIfValid(null, itemStack);
					item[0] = ReflectionUtil.newCall().getMethod(MinecraftReflectionProvider.NMS_ITEMSTACK, "getName")
							.get().invokeIfValid(nmsItemStack);
				});
		return item[0];
	}*/

	/**
	 * Send a message to a player
	 * 
	 * @param player
	 *            Player that receives message
	 * @param path
	 *            Path of message in config
	 */
	public static void msg(Player player, String path) {
		Main main = Main.getMain();
		String message = main.getConfig().getString(path);
		if (message != null) {
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
		} else {
			debug("Message path not found: " + path);
		}
	}

	/**
	 * Send a message to a player
	 * 
	 * @param player
	 *            Player that receives message
	 * @param message
	 *            message to be sent
	 */
	public static void message(Player player, String message) {
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));

	}

	/**
	 * Send a message to a player
	 * 
	 * @param player
	 *            Player that receives message
	 * @param path
	 *            Path to message in config
	 * @param repl
	 *            Replace this string
	 * @param with
	 *            With this string
	 */
	public static void msg(Player player, String path, String repl, String with) {
		Main main = Main.getMain();
		String message = main.getConfig().getString(path);
		if (message != null) {
			message = message.replaceAll(repl, with);
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
		} else {
			debug("Message path not found: " + path);
		}
	}

	/**
	 * Trim a string to a specific length.
	 * 
	 * @param string
	 *            string to trim
	 * @param maxLength
	 *            max length of string
	 * @return new trimmed string
	 */
	public static String trim(String string, int maxLength) {
		if (string.length() > 32) {
			String newString = "";
			for (int i = 0; i < string.length(); i++) {
				if (i < maxLength) {
					newString += string.charAt(i);

				}
			}
			debug(newString);
			return newString;
		} else {
			return string;
		}
	}

	/**
	 * Make a string invisible
	 * 
	 * @param string
	 *            string to make invisible
	 * @return new invisble string
	 */
	public static String HideString(String string) {
		StringBuilder builder = new StringBuilder();

		for (char c : string.toCharArray()) {
			builder.append(ChatColor.COLOR_CHAR).append(c);
		}
		return builder.toString();
	}

	/**
	 * Convert a location string to a Location object
	 * 
	 * @param locString
	 *            String containing location data
	 * @return new Location
	 */
	public static Location StringToLocation(String locString) {
		return new Location(Bukkit.getWorld(locString.split(" ")[0]), Double.parseDouble(locString.split(" ")[1]),
				Double.parseDouble(locString.split(" ")[2]), Double.parseDouble(locString.split(" ")[3]));
	}

	/**
	 * Convert a Location object to a String for storage
	 * 
	 * @param location
	 *            Location to convert
	 * @return Location String
	 */
	public static String LocationToString(Location location) {
		return location.getWorld().getName() + " " + location.getBlockX() + " " + location.getBlockY() + " "
				+ location.getBlockZ();
	}
}
